using Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data
{
    public class EntityContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<FoodBankManager> FoodBankManagers { get; set; }
        public DbSet<FoodBank> FoodBanks { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<TimeSlot> TimeSlots { get; set; }



        public EntityContext(DbContextOptions<EntityContext> options) : base(options) { }
    }
}