using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {

        private EntityContext _entityContext;

        public OrderRepository(EntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        public async Task<int> AddOrder(Order order)
        {
            _entityContext.Orders.Add(order);
            await _entityContext.SaveChangesAsync();
            
            return order.OrderId;
        }

        public Task<List<Order>> GetOrdersByFoodBankManagerId(int foodBankManagerId)
        {
            return _entityContext.Orders.Where(order => order.TimeSlot.FoodBank.FoodBankManager.FoodBankManagerId == foodBankManagerId).ToListAsync();
        }
    }
}