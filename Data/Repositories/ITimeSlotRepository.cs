using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Data.Repositories
{
    public interface ITimeSlotRepository
    {
        Task<int> AddTimeSlot(TimeSlot timeSlot);
    }
}