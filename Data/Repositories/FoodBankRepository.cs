using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Data;
using Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data.Repositories
{
    public class FoodBankRepository : IFoodBankRepository
    {

        private EntityContext _entityContext;

        public FoodBankRepository(EntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        public async Task<int> AddFoodBank(FoodBank foodBank)
        {
            _entityContext.FoodBanks.Add(foodBank);
            await _entityContext.SaveChangesAsync();

            return foodBank.FoodBankId;
        }

        public async Task<List<FoodBank>> GetAllFoodBanks()
        {
            return await _entityContext.FoodBanks.ToListAsync();
        }

        public async Task<List<FoodBank>> GetFoodBanksWhereZipCodeStartsWith(string zipCode)
        {
            return await _entityContext.FoodBanks.Where(foodBank => foodBank.ZipCode.StartsWith(zipCode)).ToListAsync();
        }
    }
}