using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Data.Repositories
{
    public interface IFoodBankRepository
    {
        Task<int> AddFoodBank(FoodBank foodBank);
        Task<List<FoodBank>> GetFoodBanksWhereZipCodeStartsWith(string zipCode);

        Task<List<FoodBank>> GetAllFoodBanks();
    }
}