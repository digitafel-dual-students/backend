using System.Threading.Tasks;
using Backend.Data;
using Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data.Repositories
{
    public class FoodBankManagerRepository : IFoodBankManagerRepository
    {

        private EntityContext _entityContext;

        public FoodBankManagerRepository(EntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        public async Task<FoodBankManager> GetFoodBankManagerByEmail(string email)
        {
            // return from foodBankManager in _entityContext.FoodBankManagers
            //        join user in _entityContext.Users
            //        on 
            var user = await _entityContext.Users.SingleAsync(user => user.Email == email);
            return await _entityContext.FoodBankManagers.SingleAsync(foodBankManager => foodBankManager.UserId == user.UserId);
        }
    }
}