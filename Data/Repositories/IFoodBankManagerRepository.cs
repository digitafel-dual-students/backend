using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Data.Repositories
{
    public interface IFoodBankManagerRepository
    {
        Task<FoodBankManager> GetFoodBankManagerByEmail(string email);
    }
}