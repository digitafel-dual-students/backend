using System.Collections.Generic;
using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Data.Repositories
{
    public interface IOrderRepository
    {
        Task<List<Order>> GetOrdersByFoodBankManagerId(int foodBankManagerId);

        Task<int> AddOrder(Order order);
    }
}