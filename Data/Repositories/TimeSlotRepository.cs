using System.Threading.Tasks;
using Backend.Entities;

namespace Backend.Data.Repositories
{
    public class TimeSlotRepository : ITimeSlotRepository
    {
        private EntityContext _entityContext;

        public TimeSlotRepository(EntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        public async Task<int> AddTimeSlot(TimeSlot timeSlot)
        {
            _entityContext.TimeSlots.Add(timeSlot);
            await _entityContext.SaveChangesAsync();

            return timeSlot.TimeSlotId;
        }
    }
}