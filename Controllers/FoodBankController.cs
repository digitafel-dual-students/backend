using System;
using System.Threading.Tasks;
using Backend.Data.Repositories;
using Backend.DTOs;
using Backend.Data;
using Backend.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FoodBankController : Controller
    {
        
        private IFoodBankRepository _foodBankRepository;

        private EntityContext _entityContext;
        public FoodBankController(IFoodBankRepository foodBankRepository, EntityContext entityContext)
        {
            _foodBankRepository = foodBankRepository;
            _entityContext = entityContext;
        }

        [HttpGet]
        public async Task<IActionResult> GetFoodBanks([FromQuery(Name="zipCode")] string zipCode)
        {
            if(String.IsNullOrWhiteSpace(zipCode))
            {
                return Ok(await _foodBankRepository.GetAllFoodBanks());
            }
            return Ok(await _foodBankRepository.GetFoodBanksWhereZipCodeStartsWith(zipCode));
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> AddFoodBank([FromBody] CreateFoodBankDTO createFoodBankDTO)
        {
            try
            {
                var foodBankManager = await _entityContext.FoodBankManagers.SingleAsync(manager => manager.FoodBankManagerId == createFoodBankDTO.FoodBankManagerId);
                var foodBank = new FoodBank() 
                {
                    City = createFoodBankDTO.City,
                    Homepage = createFoodBankDTO.Homepage,
                    StreetName = createFoodBankDTO.StreetName,
                    StreetNumber = createFoodBankDTO.StreetNumber,
                    Text = createFoodBankDTO.Text,
                    ZipCode = createFoodBankDTO.ZipCode,
                    FoodBankManager = foodBankManager
                };
                int foodBankId = await _foodBankRepository.AddFoodBank(foodBank);
                return Ok(new {foodBankId});
            }
            catch (System.Exception)
            {
                throw;
            }
        }

    }
}