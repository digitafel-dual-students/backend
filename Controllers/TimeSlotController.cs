using System.Linq;
using System.Threading.Tasks;
using backend.DTOs;
using Backend.Data;
using Backend.Data.Repositories;
using Backend.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TimeSlotController : Controller
    {

        private ITimeSlotRepository _timeSlotRepository;
        private EntityContext _entityContext;

        public TimeSlotController(ITimeSlotRepository timeSlotRepository, EntityContext entityContext)
        {
            _timeSlotRepository = timeSlotRepository;
            _entityContext = entityContext;
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> AddTimeSlot([FromBody] CreateTimeSlotDTO createTimeSlotDTO)
        {
            var foodBank = await _entityContext.FoodBanks.SingleOrDefaultAsync(foodBank => foodBank.FoodBankId == createTimeSlotDTO.FoodBankId);
            var timeSlot = new TimeSlot()
            {
                Start = createTimeSlotDTO.Start,
                End = createTimeSlotDTO.End,
                MaxParticipants = createTimeSlotDTO.MaxParticipants,
                FoodBank = foodBank
            };
            int timeSlotId = await _timeSlotRepository.AddTimeSlot(timeSlot);
            return Ok(new {timeSlotId});
        }
    }
}