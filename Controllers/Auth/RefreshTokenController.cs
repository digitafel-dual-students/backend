using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Backend.Data;
using Backend.Services.Auth;
using Backend.DTOs;

namespace Backend.Controllers.Auth
{

    [ApiController]
    [Route("[controller]")]
    public class RefreshTokenController : Controller
    {
        
        private readonly ITokenService _tokenService;
        private readonly EntityContext _entityContext;

        public RefreshTokenController(ITokenService tokenService, EntityContext entityContext)
        {
            _tokenService = tokenService;
            _entityContext = entityContext;
        }

        [HttpPost]
        public async Task<IActionResult> Refresh(RefreshTokenRequestDTO refreshTokenRequest) 
        {
            var principal = _tokenService.GetPrincipalFromExpiredToken(refreshTokenRequest.Token);
            var username = principal.Identity.Name;

            var user = _entityContext.Users.SingleOrDefault(user => user.Email == username);
            if (user == null || user.RefreshToken != refreshTokenRequest.RefreshToken)
            {
                return BadRequest();
            }

            var newJwt = _tokenService.GenerateAccessToken(principal.Claims);
            var newRefreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;
            await _entityContext.SaveChangesAsync();

            return new ObjectResult(new
            {
                token = newJwt,
                refreshToken = newRefreshToken
            });
        }



    }
}