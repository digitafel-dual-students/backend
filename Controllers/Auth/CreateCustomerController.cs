using System.Linq;
using System.Threading.Tasks;
using backend.ExceptionHandling;
using backend.Services;
using Backend.Data;
using Backend.DTOs;
using Backend.Entities;
using Backend.Services.Auth;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers.Auth
{
    [ApiController]
    [Route("[controller]")]
    public class CreateCustomerController : Controller
    {
        private IUserService _userService;

        public CreateCustomerController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Signup(CreateCustomerRequestDTO createCustomerRequestDTO)
        {
            if (await _userService.UserExists(createCustomerRequestDTO.Email))
            {
                throw new ObjectAlreadyExistsException();
            }

            var user = new User
            {
                Email = createCustomerRequestDTO.Email,
                Password = createCustomerRequestDTO.Password,
                FirstName = createCustomerRequestDTO.FirstName,
                LastName = createCustomerRequestDTO.LastName,
            };

            var customer = new Customer()
            {
                User = user,
                NumberOfAdults = createCustomerRequestDTO.NumberOfAdults,
                NumberOfChildren = createCustomerRequestDTO.NumberOfChildren,
                Preferences = createCustomerRequestDTO.Preferences
            };

            var customerId = await _userService.CreateCustomer(user, customer, createCustomerRequestDTO.FoodBankId);

            return Ok(user);
        }
    }
}