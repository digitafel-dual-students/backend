using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Backend.Data;
using Backend.DTOs;
using Backend.Services.Auth;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers.Auth
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : Controller
    {

        private readonly EntityContext _entityContext;
        private readonly IPasswordHasher _passwordHasher;
        private readonly ITokenService _tokenService;

        public LoginController(EntityContext entityContext, IPasswordHasher passwordHasher, ITokenService tokenService) 
        {
            _entityContext = entityContext;
            _passwordHasher = passwordHasher;
            _tokenService = tokenService;
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginRequestDTO loginRequest)
        {
            var user = _entityContext.Users.SingleOrDefault(user => user.Email == loginRequest.Username);
            if (user == null || !_passwordHasher.VerifyIdentityV3Hash(loginRequest.Password, user.Password))
            {
                return BadRequest();
            }

            var usersClaims = new[]
            {
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString())
            };

            var jwt = _tokenService.GenerateAccessToken(usersClaims);
            var refreshToken = _tokenService.GenerateRefreshToken();

            user.RefreshToken = refreshToken;
            await _entityContext.SaveChangesAsync();

            return new ObjectResult(new
            {
                token = jwt,
                refreshToken = refreshToken
            });
        }

    }
}