using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Backend.Data;

namespace Backend.Controllers.Auth
{

    [ApiController]
    [Route("[controller]")]
    public class RevokeTokenController : Controller
    {

        private readonly EntityContext _entityContext;

        public RevokeTokenController(EntityContext entityContext)
        {
            _entityContext = entityContext;
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> Revoke()
        {
            var email = User.Identity.Name;

            var user = _entityContext.Users.SingleOrDefault(user => user.Email == email);
            if (user == null)
            {
                return BadRequest();
            }

            user.RefreshToken = null;

            await _entityContext.SaveChangesAsync();

            return NoContent();
        }
    }
}