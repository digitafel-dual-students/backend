using System.Linq;
using System.Threading.Tasks;
using Backend.DTOs;
using Backend.Data;
using Backend.Entities;
using Microsoft.AspNetCore.Mvc;
using Backend.Services.Auth;
using backend.Services;
using backend.ExceptionHandling;

namespace Backend.Controllers.Auth
{
    [ApiController]
    [Route("[controller]")]
    public class CreateFoodBankManagerController : Controller
    {

        private IUserService _userService;
        
        public CreateFoodBankManagerController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Signup(CreateFoodBankManagerDTO createFoodBankManagerDTO)
        {
            if (await _userService.UserExists(createFoodBankManagerDTO.Email))
            {
                throw new ObjectAlreadyExistsException();
            }

            var user = new User
            {
                Email = createFoodBankManagerDTO.Email,
                Password = createFoodBankManagerDTO.Password,
                FirstName = createFoodBankManagerDTO.FirstName,
                LastName = createFoodBankManagerDTO.LastName
            };

            var foodBankManager = new FoodBankManager() 
            {
                User = user
            };

            return Ok(foodBankManager);
        }

    }
}