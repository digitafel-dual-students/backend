using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Backend.Data.Repositories;
using backend.DTOs;
using Backend.Entities;
using Backend.Data;
using Microsoft.EntityFrameworkCore;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderController : Controller
    {

        private IOrderRepository _orderRepository;
        private IFoodBankManagerRepository _foodBankManagerRepository;
        private EntityContext _entityContext;
        
        public OrderController(IOrderRepository orderRepository, IFoodBankManagerRepository foodBankManagerRepository, EntityContext entityContext)
        {
            _orderRepository = orderRepository;
            _foodBankManagerRepository = foodBankManagerRepository;
            _entityContext = entityContext;
        }

        [HttpGet, Authorize]
        public async Task<IActionResult> GetOrders()
        {
            var email = User.Identity.Name;
            var foodBankManager = await _foodBankManagerRepository.GetFoodBankManagerByEmail(email);

            var orders = await _orderRepository.GetOrdersByFoodBankManagerId(foodBankManager.FoodBankManagerId);

            return Ok(orders);
        }

        [HttpPost, Authorize]
        public async Task<IActionResult> AddOrder([FromBody] CreateOrderDTO createOrderDTO)
        {
            var email = User.Identity.Name;
            var customer = await _entityContext.Customers.SingleAsync(customer => customer.User.Email == email);
            var order = new Order()
            {
                Comment = createOrderDTO.Comment,
                TimeSlotId = createOrderDTO.TimeSlotId,
                Customer = customer
            };
            int orderId = await _orderRepository.AddOrder(order);
            return Ok(new {orderId});
        }

    }
}