﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Backend.Migrations
{
    public partial class AddedForeignKeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_FoodBanks_FoodBankId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Users_UserId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodBankManagers_Users_UserId",
                table: "FoodBankManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_TimeSlots_TimeSlotId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlots_FoodBanks_FoodBankId",
                table: "TimeSlots");

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "TimeSlots",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TimeSlotId",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankManagerId",
                table: "FoodBanks",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "FoodBankManagers",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Customers",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "Customers",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_FoodBanks_FoodBankId",
                table: "Customers",
                column: "FoodBankId",
                principalTable: "FoodBanks",
                principalColumn: "FoodBankId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Users_UserId",
                table: "Customers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBankManagers_Users_UserId",
                table: "FoodBankManagers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks",
                column: "FoodBankManagerId",
                principalTable: "FoodBankManagers",
                principalColumn: "FoodBankManagerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_TimeSlots_TimeSlotId",
                table: "Orders",
                column: "TimeSlotId",
                principalTable: "TimeSlots",
                principalColumn: "TimeSlotId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlots_FoodBanks_FoodBankId",
                table: "TimeSlots",
                column: "FoodBankId",
                principalTable: "FoodBanks",
                principalColumn: "FoodBankId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_FoodBanks_FoodBankId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Users_UserId",
                table: "Customers");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodBankManagers_Users_UserId",
                table: "FoodBankManagers");

            migrationBuilder.DropForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_TimeSlots_TimeSlotId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_TimeSlots_FoodBanks_FoodBankId",
                table: "TimeSlots");

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "TimeSlots",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TimeSlotId",
                table: "Orders",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "Orders",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankManagerId",
                table: "FoodBanks",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "FoodBankManagers",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "Customers",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "Customers",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_FoodBanks_FoodBankId",
                table: "Customers",
                column: "FoodBankId",
                principalTable: "FoodBanks",
                principalColumn: "FoodBankId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_Users_UserId",
                table: "Customers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBankManagers_Users_UserId",
                table: "FoodBankManagers",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "UserId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks",
                column: "FoodBankManagerId",
                principalTable: "FoodBankManagers",
                principalColumn: "FoodBankManagerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Customers_CustomerId",
                table: "Orders",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "CustomerId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_TimeSlots_TimeSlotId",
                table: "Orders",
                column: "TimeSlotId",
                principalTable: "TimeSlots",
                principalColumn: "TimeSlotId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_TimeSlots_FoodBanks_FoodBankId",
                table: "TimeSlots",
                column: "FoodBankId",
                principalTable: "FoodBanks",
                principalColumn: "FoodBankId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
