﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Backend.Migrations
{
    public partial class AddedForeignKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks");

            migrationBuilder.DropIndex(
                name: "IX_FoodBanks_FoodBankManagerId",
                table: "FoodBanks");

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "FoodBanks",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankId",
                table: "FoodBanks",
                column: "FoodBankId",
                principalTable: "FoodBankManagers",
                principalColumn: "FoodBankManagerId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankId",
                table: "FoodBanks");

            migrationBuilder.AlterColumn<int>(
                name: "FoodBankId",
                table: "FoodBanks",
                type: "integer",
                nullable: false,
                oldClrType: typeof(int))
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.CreateIndex(
                name: "IX_FoodBanks_FoodBankManagerId",
                table: "FoodBanks",
                column: "FoodBankManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodBanks_FoodBankManagers_FoodBankManagerId",
                table: "FoodBanks",
                column: "FoodBankManagerId",
                principalTable: "FoodBankManagers",
                principalColumn: "FoodBankManagerId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
