namespace Backend.DTOs
{
    public class CreateFoodBankDTO
    {
        public string StreetName { get; set; }

        public string StreetNumber { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Homepage { get; set; }

        public string Text { get; set; }

        public int FoodBankManagerId { get; set; }
    }
}