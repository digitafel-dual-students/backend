namespace Backend.DTOs
{
    public class CreateCustomerRequestDTO
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public int NumberOfAdults { get; set; }

        public int NumberOfChildren { get; set; }

        public int FoodBankId { get; set; }

        public string Preferences { get; set; }

    }
}