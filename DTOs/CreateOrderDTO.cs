namespace backend.DTOs
{
    public class CreateOrderDTO
    {
        public int TimeSlotId { get; set; }
        public string Comment { get; set; }
    }
}