using System;

namespace backend.DTOs
{
    public class CreateTimeSlotDTO
    {
        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int MaxParticipants { get; set; }

        public int FoodBankId { get; set; }

    }
}