using System.Threading.Tasks;
using Backend.Data;
using Backend.Entities;
using Backend.Services.Auth;
using Microsoft.EntityFrameworkCore;

namespace backend.Services
{
    public class UserService : IUserService
    {
        
        private EntityContext _entityContext;
        private IPasswordHasher _passwordHasher;

        public UserService(EntityContext entityContext, IPasswordHasher passwordHasher)
        {
            _entityContext = entityContext;
            _passwordHasher = passwordHasher;
        }

        public async Task<int> CreateCustomer(User user, Customer customer, int foodBankId)
        {
            user.Password = _passwordHasher.GenerateIdentityV3Hash(user.Password);
            _entityContext.Users.Add(user);
            await _entityContext.SaveChangesAsync();

            var foodBank = await _entityContext.FoodBanks.SingleAsync(foodBank => foodBank.FoodBankId == foodBankId);

            customer.FoodBank = foodBank;

            _entityContext.Customers.Add(customer);
            await _entityContext.SaveChangesAsync();

            return customer.CustomerId;
        }

        public async Task<int> CreateFoodBankManager(User user, FoodBankManager foodBankManager)
        {
            user.Password = _passwordHasher.GenerateIdentityV3Hash(user.Password);
            _entityContext.Users.Add(user);
            await _entityContext.SaveChangesAsync();

            _entityContext.FoodBankManagers.Add(foodBankManager);
            await _entityContext.SaveChangesAsync();

            return foodBankManager.FoodBankManagerId;
        }

        public async Task<bool> UserExists(string email)
        {
            return await _entityContext.Users.AnyAsync(user => user.Email == email);
        }

    }
}