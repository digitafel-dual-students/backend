using System.Threading.Tasks;
using Backend.Entities;

namespace backend.Services
{
    public interface IUserService
    {
        Task<bool> UserExists(string email);
        Task<int> CreateCustomer(User user, Customer customer, int foodBankId);

        Task<int> CreateFoodBankManager(User user, FoodBankManager foodBankManager);
    }
}