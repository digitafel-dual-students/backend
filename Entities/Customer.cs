namespace Backend.Entities
{
    public class Customer
    {
        public int CustomerId { get; set; }

        public int FoodBankId { get; set; }
        public FoodBank FoodBank { get; set; }

        public int NumberOfAdults { get; set; }

        public int NumberOfChildren { get; set; }

        public string Preferences { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}