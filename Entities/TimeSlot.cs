using System;
using System.Collections.Generic;

namespace Backend.Entities
{
    public class TimeSlot
    {
        public int TimeSlotId { get; set; }

        public DateTime Start { get; set; }

        public DateTime End { get; set; }

        public int MaxParticipants { get; set; }

        public int FoodBankId { get; set; }
        public FoodBank FoodBank { get; set; }

        public List<Order> Orders { get; set; }
    }
}