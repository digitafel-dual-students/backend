using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Entities
{
    public class FoodBank
    {
        [ForeignKey(nameof(FoodBankManager))]
        public int FoodBankId { get; set; }

        public string Homepage { get; set; }

        public string StreetName { get; set; }

        public string StreetNumber { get; set; }

        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Text { get; set; }

        public int FoodBankManagerId { get; set; }
        public FoodBankManager FoodBankManager { get; set; }

        public List<Customer> Customers { get; set; }

        public List<TimeSlot> TimeSlots { get; set; }
    }
}