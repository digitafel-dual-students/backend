using System.Collections.Generic;

namespace Backend.Entities
{
    public class FoodBankManager
    {
        public int FoodBankManagerId { get; set; }

        public List<FoodBank> FoodBanks { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}